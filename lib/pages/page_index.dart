import 'package:cup_game_app/components/cup_choice_item.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  bool _isStart = false; // 시작했냐
  num pandon = 100;
  List<num> result = [0, 100, 200];

  void _startGame(){
    setState(() {
      _isStart = true;
      result.shuffle();
      print(result);
    });
  }

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
            'Ga Cha Game',
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_isStart) {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(10, 200, 0, 0),
              child: Row(
                children: [
                  CupChoiceItem(pandon: pandon, currentMoney: result[0]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[1]),
                  CupChoiceItem(pandon: pandon, currentMoney: result[2]),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 25, 0, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.lightBlueAccent,
                  onPrimary: Colors.white,
                  elevation: 10.0,
                  minimumSize: const Size(356, 60),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  )
                ),
                onPressed: () {
                  setState(() {
                    _isStart = false;
                  });
                },
                child: const Text(
                    '종료',
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white
                    ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 300, 0, 0),
              child: Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.lightBlueAccent,
                    onPrimary: Colors.white,
                    elevation: 10.0,
                    minimumSize: const Size(356, 60),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onPressed: () {
                    _startGame();
                  },
                  child: const Text(
                      '시작',
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.white
                  ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }
}
